package services

import (
	"fmt"
	"gopkg.in/redis.v4"
	"os"
	re "github.com/kataras/iris/v12/sessions/sessiondb/redis"
)

var redisdb *redis.Client
var re2 *re.Database

func redisinit()  {
	Addr,err:=cfg.GetValue("redis","addr")
	if err != nil{
		return
	}
	Password,_:=cfg.GetValue("redis","pass")
	client := redis.NewClient(&redis.Options{
		Addr:     Addr,
		Password: Password,
		DB:       0,
	})

	_, err = client.Ping().Result()
	if(err != nil){
		fmt.Println("Redis 服务器连接失败")
		os.Exit(0)
	}

	re2=re.New(re.Config{
		Network: "tcp",
		Addr:     Addr,
		Password: Password,
		Database: "",
	})

	redisdb=client
}