package services

import (
	"fmt"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/hero"
	"github.com/xormplus/xorm"
	"gitee.com/q9uo11/webmodel/util/ajax"
)

func DbHandler(fs ... iris.Handler) []iris.Handler {
	var handlers=[]iris.Handler{hero.Handler(dbHandler)}
	handlers=append(handlers,fs...)
	return fs
}


func Handler(fs ... iris.Handler) []iris.Handler  {
	var handlers=[]iris.Handler{}
	handlers=append(handlers,fs...)
	return fs
}


func dbHandler(ctx iris.Context,session xorm.Session)  {
	defer func() {
		if x := recover(); x != nil {

			err := fmt.Errorf("%v", x)

			e :=session.Rollback()
			if e != nil{
				panic(e.Error())
			}

			panic(err.Error())
		}else {
			e :=session.Commit()

			if e != nil{
				panic(e.Error())
			}
		}
	}()
	ctx.Next()

}
var ErrorHandler=func(ctx iris.Context)  {
	defer func() {
		if x := recover(); x != nil {
			if ctx.IsStopped() {
				return
			}
			err := fmt.Errorf("%v", x)

			if err.Error() == "未登录"{
				ctx.JSON(ajax.AjaxResult{Code: -1000,Msg: err.Error()})
			}else {
				ctx.JSON(ajax.AjaxResult{}.AddError(err.Error()))

			}

		}
	}()
	ctx.Next()
}