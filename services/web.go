package services

import (
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/hero"
	"github.com/kataras/iris/v12/sessions"
	"github.com/xormplus/xorm"
	"net/http"
	"time"
	_ "github.com/go-sql-driver/mysql" //注入mysql
	"gitee.com/q9uo11/webmodel/views"
)

var funs=[]func(){}

func Register(f func())  {
	funs=append(funs,f)
}

var bfuns=[]func(){}
func RegisterBefore(f func())  {
	bfuns=append(bfuns,f)
}


var  app = iris.New()



var debug =true

func Getapp()  *iris.Application{
	return app
}

func Getdb()  *xorm.Engine{
	return db
}

func Offdebug()  {
	debug=false
}


func Runweb()  {
    app.Use(ErrorHandler)
	mysql() // 初始化数据库

	redisinit() //初始化redis
	
	if debug{
		app.Logger().SetLevel("debug")
	}

	sessionManager := sessions.New(sessions.Config{
		Cookie:       "site_session_id",
		Expires:      3*60 * time.Minute,
		AllowReclaim: true,
	})

	if redisdb != nil && debug{
		println("session redis 已启动")
	}

	//注册session
	hero.Register(sessionManager.Start)

	//注册数据库
	hero.Register(db)

	if redisdb != nil{

		sessionManager.UseDatabase(re2)

		//注册redis
		hero.Register(redisdb)

	}



	hero.Register(func(ctx iris.Context) *xorm.Session {
		return db.NewSession()
	})


	app.OnErrorCode(iris.StatusNotFound, func(ctx iris.Context) {
		if ctx.Request().RequestURI == "" || ctx.Request().RequestURI == "/"{

			ctx.StatusCode(http.StatusOK)
			ctx.StopExecution()

			ctx.HTML(string(views.Default))
		}else {
			ctx.HTML(string(views.Nofound))
		}

	})


	//此处可以注册路由或者中间件
	for i:=0;i< len(bfuns);i++{
		bfuns[i]()
	}

	for i:=0;i< len(funs);i++{
		funs[i]()
	}

	app.Run(
		//在localhost：8080启动Web服务器
		iris.Addr(":"+port),
		//启用更快的json序列化和优化：
		iris.WithOptimizations,
	)

}


