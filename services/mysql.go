package services

import (
	"github.com/xormplus/xorm"
	"os"
	"gitee.com/q9uo11/webmodel/datamodels"
)

func mysql()  {
	mysqluser,_:=cfg.GetValue("mysql","user")
	mysqlpass,_:=cfg.GetValue("mysql","pass")
	mysqlurls,_:=cfg.GetValue("mysql","url")
	mysqldb,_:=cfg.GetValue("mysql","db")
	mysqlport,_:=cfg.GetValue("mysql","port")
	var mysqlu=mysqluser+":"+mysqlpass+"@tcp("+mysqlurls+":"+mysqlport+")/"+mysqldb+"?charset=utf8&parseTime=true&loc=Asia%2FShanghai"
	//println(mysqlu)
	engine, err := xorm.NewEngine("mysql",mysqlu)
	if(err != nil){
		println("初始化数据库失败")
		println(err.Error())
		os.Exit(0)
	}


	if ( debug ) {
		//fmt.Println("检测到当前为debug模式，数据库Sql反馈已开启")
		engine.ShowSQL(true)
	}


	db=engine
	if debug{
		println("数据库初始完毕")
	}

	//fmt.Printf("%v",static.DbModels)
	//对数据库缺少字段自动补齐
	for i:= 0;i<len(datamodels.DbModels);i++{
		var model=datamodels.DbModels[i]
		//fmt.Printf("%v",static.DbModels[i])
		err := engine.Sync(model)
		if err != nil{
			println("初始化模型失败")
			println(err.Error())
			os.Exit(0)
		}
	}
	if debug{
		println("数据模型初始完毕")
	}
}
