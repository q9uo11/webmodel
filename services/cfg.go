package services

import (
	"github.com/Unknwon/goconfig"
	"github.com/xormplus/xorm"
	"os"
)

var confpath="app.ini"

var db *xorm.Engine

var cfg *goconfig.ConfigFile

var port=""

func init()  {
	c, err := goconfig.LoadConfigFile(confpath)
	if(err != nil){
		println("读取配置文件失败")
		os.Exit(0)
	}

	cfg=c
	port,_=cfg.GetValue("web","port")
	if port == ""{
		println("获取监听端口错误")
		os.Exit(0)
	}
}


