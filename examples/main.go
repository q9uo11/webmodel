package main

import (
	"os"
	"os/signal"
	"syscall"
	"gitee.com/q9uo11/webmodel/services"
)

func main()  {
	signalChan := make(chan os.Signal, 1)
	go func() {
		//阻塞程序运行，直到收到终止的信号
		<-signalChan
		os.Exit(0)
	}()
	signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM)
	go services.Runweb()
	select {} // 阻塞
}
