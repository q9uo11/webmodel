package web

import (
	"github.com/kataras/iris/v12"
	"github.com/xormplus/xorm"
	"gitee.com/q9uo11/webmodel/services"
)

func Getapp()  *iris.Application{
	return services.Getapp()
}

func Getdb()  *xorm.Engine{
	return services.Getdb()
}

func Offdebug() {
	services.Offdebug()
}