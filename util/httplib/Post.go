package httplib

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"
)

var c = &http.Client{
	Timeout: 15 * time.Second,
}


func Post(posturl string,params map[string]string) string  {
	param := url.Values{}
	for k, v := range params{
		param.Add(k,v)
	}
	bytes_req := []byte(param.Encode())
	resp, err := c.Post(posturl, "application/x-www-form-urlencoded", bytes.NewReader(bytes_req))
	if err != nil {
		panic(err.Error())
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		panic(err.Error())
	}
	return string(body)
}

