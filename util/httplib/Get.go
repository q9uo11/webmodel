package httplib

import (
	"io/ioutil"
	"net/http"
)

func Get(url string) string {
	resp, err := http.Get(url)
	//resp.Header.Set("Referrer","http://ms.xmheigu.com/zentao")
	if err != nil {
		return  ""
	}
	//println(resp.Header.Get("Referrer"))
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return  ""
	}
	return string(body)
}
