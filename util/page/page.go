package page

import (
	"github.com/xormplus/xorm"
	"log"
	"reflect"
	"gitee.com/q9uo11/webmodel/services"
	"gitee.com/q9uo11/webmodel/util/DateKit"
	"gitee.com/q9uo11/webmodel/util/stringKit"
	"gitee.com/q9uo11/webmodel/util/sql"
)


type SqlController func(iscount bool,ma map[string]interface{})  (string,error)

type ChaController func(db *xorm.Session)  *xorm.Session

type QueryPage struct {
	Code int         `json:"code"`
	Msg  string      `json:"msg"`
	Data  []map[string]interface{} `json:"data"`
	Count int64  `json:"count"`
	Page int64 `json:"page"`
}

type ObjQueryPage struct {
	Code int         `json:"code"`
	Msg  string      `json:"msg"`
	Data  interface{} `json:"data"`
	Count int64  `json:"count"`
	Page int64 `json:"page"`
}

var errorpage=QueryPage{Code:0,Page:1,Data:[]map[string]interface{}{}}



func PageAjaxByOrm(cha ChaController,page int64,limit int64,order string,obj interface{}) ObjQueryPage {
	t := reflect.TypeOf(obj)
	if t.Kind() != reflect.Ptr {
		panic(" obj is not ptr")
	}
	t = t.Elem()
	if t.Kind() != reflect.Slice {
		panic(" obj is not slice")
	}
	countdb:=cha(services.Getdb().NewSession())
	count,err := (countdb).Select("count(*)").Count(reflect.New(t.Elem()).Interface())
	if(err != nil){
		panic(err)
	}
	if(page <=0 ){
		page=1
	}
	if(limit ==0 ){
		limit=20
	}
	if(count == 0){
		return ObjQueryPage{Code:0,Page:page,Data:[]map[string]interface{}{}}
	}else {
		db:=cha(services.Getdb().NewSession())
		if(order != ""){
			sql.CheckOrder(order)
			db.OrderBy(order)
		}
		var rowBegin=(page-1)*limit
		err:=db.Limit(int(limit),int(rowBegin)).Find(obj)
		if(err != nil){
			panic(err)
		}
		if(reflect.ValueOf(obj).Elem().Len() <= 0){
			db:=cha(services.Getdb().NewSession())
			if(order != ""){
				sql.CheckOrder(order)
				db.OrderBy(order)
			}
			err:=db.Limit(int(limit)).Find(obj)
			if(err != nil){
				panic(err)
			}
			return ObjQueryPage{Code:0,Page:1,Count:count,Data:obj}
		}else {
			return ObjQueryPage{Code:0,Page:page,Count:count,Data:obj}
		}
	}
}

func PageAjax(ma map[string]interface{},controller SqlController) QueryPage {
	countsql,err:=controller(true,ma)
	if(err != nil){
		return errorpage
	}
	//log.Println(countsql)
	var count int64=0
	l,err:=services.Getdb().SQL(countsql,&ma).Query().List()
	if(err != nil){
		log.Println(err)
		return errorpage
	}
	if(len(l)>0){
		count=stringKit.ToInt(l[0]["count"])
	}
	var page=ma["page"].(int64)
	var limit=ma["limit"].(int64)
	if(page <=0 ){
		page=1
	}
	if(limit ==0 ){
		limit=20
	}
	if(count == 0){
		return QueryPage{Code:0,Page:page,Data:[]map[string]interface{}{}}
	}else {
		sql,err:=controller(false,ma)
		println(sql)
		if(err != nil){
			return errorpage
		}
		var rowBegin=(page-1)*limit
		var chasql=sql+" LIMIT "+stringKit.ToString(rowBegin)+","+stringKit.ToString(limit)
		//log.Println(chasql)
		l,err:=services.Getdb().SQL(chasql,&ma).QueryWithDateFormat(DateKit.Datetime).List()
		if(err != nil){
			panic(err)
		}
		if(len(l) <= 0){
			var chasql=sql+" LIMIT "+stringKit.ToString(limit)
			//log.Println(chasql)
			l,err:=services.Getdb().SQL(chasql,&ma).QueryWithDateFormat(DateKit.Datetime).List()
			if(err != nil){
				panic(err)
			}
			return QueryPage{Code:0,Page:1,Count:count,Data:l}
		}else {
			return QueryPage{Code:0,Page:page,Count:count,Data:l}
		}
	}

}

