package stringKit

import (
	"encoding/json"
	"errors"
	"fmt"
	"reflect"
	"strconv"
)

func ToString(obj interface{}) string {

	switch obj.(type) {
	case string:
		return obj.(string);
	case int64:
		return strconv.FormatInt(obj.(int64),10);
	case int:
		return ToString(int64(obj.(int)))
	case int8:
		return ToString(int64(obj.(int8)))
	case int16:
		return ToString(int64(obj.(int16)))
	case int32:
		return ToString(int64(obj.(int32)))
	case float64:
		return strconv.FormatFloat(obj.(float64), 'f', -1, 64)
	case float32:
		return ToString(int64(obj.(float32)))//strconv.FormatFloat(float64(obj.(float32)), 'f', -1, 64)
	case bool:
		if(obj.(bool)){
			return "true"
		}else {
			return "false"
		}
	case []byte:
		return string(obj.([]byte))
	case byte:
		return string(obj.(byte))
	default:
		return ToJson(obj)
	}

}

func ToJson(obj interface{})  string {
	b, err := json.Marshal(obj)
	if err != nil {
		panic(err.Error())
	}
	return string(b)
}


func Tointde(obj interface{},de int64)  int64  {
	switch obj.(type) {
	case string:
		i, err := strconv.ParseInt(obj.(string), 10, 64)
		if(err != nil){
			return de
		}
		return i
	case int64:
		return obj.(int64);
	case int:
		return ToInt(int64(obj.(int)))
	case int8:
		return ToInt(int64(obj.(int8)))
	case int16:
		return ToInt(int64(obj.(int16)))
	case int32:
		return ToInt(int64(obj.(int32)))
	case float64:
		return int64(obj.(float64))
	case float32:
		return  ToInt(float64(obj.(float32)))
	case []byte:
		return ToInt(string(obj.([]byte)))
	case byte:
		return  ToInt(string(obj.(byte)))
	default:
		return de
	}
}


func ToInt(obj interface{}) int64 {
	switch obj.(type) {
	case string:
		i, err := strconv.ParseInt(obj.(string), 10, 64)
		if(err != nil){
			panic(err.Error())
		}
		return i
	case int64:
		return obj.(int64);
	case int:
		return ToInt(int64(obj.(int)))
	case int8:
		return ToInt(int64(obj.(int8)))
	case int16:
		return ToInt(int64(obj.(int16)))
	case int32:
		return ToInt(int64(obj.(int32)))
	case float64:
		return int64(obj.(float64))
	case float32:
		return  ToInt(float64(obj.(float32)))
	case []byte:
		return ToInt(string(obj.([]byte)))
	case byte:
		return  ToInt(string(obj.(byte)))
	default:
		panic("错误类型"+ToString(obj))
	}
}


func ToIntErr(obj interface{}) (int64,error) {
	switch obj.(type) {
	case string:
		i, err := strconv.ParseInt(obj.(string), 10, 64)
		if(err != nil){
			return  0,err
		}
		return i,nil
	case int64:
		return obj.(int64),nil;
	case int:
		return ToInt(int64(obj.(int))),nil
	case int8:
		return ToInt(int64(obj.(int8))),nil
	case int16:
		return ToInt(int64(obj.(int16))),nil
	case int32:
		return ToInt(int64(obj.(int32))),nil
	case float64:
		return int64(obj.(float64)),nil
	case float32:
		return  ToInt(float64(obj.(float32))),nil
	case []byte:
		return ToInt(string(obj.([]byte))),nil
	case byte:
		return  ToInt(string(obj.(byte))),nil
	default:
		//panic()
		return  0,errors.New("错误类型"+ToString(obj))
	}
}

func ToFloat(obj interface{}) float64 {
	switch obj.(type) {
	case string:
		i, err := strconv.ParseFloat(obj.(string),64)
		if(err != nil){
			panic(err.Error())
		}
		return i
	case int64:
		return float64(obj.(int64));
	case int:
		return ToFloat(int64(obj.(int)))
	case int8:
		return ToFloat(int64(obj.(int8)))
	case int16:
		return ToFloat(int64(obj.(int16)))
	case int32:
		return ToFloat(int64(obj.(int32)))
	case float64:
		return obj.(float64)
	case float32:
		return  ToFloat(float64(obj.(float32)))
	case []byte:
		return ToFloat(string(obj.([]byte)))
	case byte:
		return  ToFloat(string(obj.(byte)))
	default:
		var t=reflect.TypeOf(obj)
		if(t.Kind() == reflect.Ptr){
			t=t.Elem()
		}
		println(t.Kind().String())
		panic("error type")
	}
}

func ToFloatde(obj interface{},de float64)  float64  {
	switch obj.(type) {
	case string:
		i, err := strconv.ParseFloat(obj.(string),64)
		if(err != nil){
			panic(err.Error())
		}
		return i
	case int64:
		return float64(obj.(int64));
	case int:
		return ToFloat(int64(obj.(int)))
	case int8:
		return ToFloat(int64(obj.(int8)))
	case int16:
		return ToFloat(int64(obj.(int16)))
	case int32:
		return ToFloat(int64(obj.(int32)))
	case float64:
		return obj.(float64)
	case float32:
		return  ToFloat(float64(obj.(float32)))
	case []byte:
		return ToFloat(string(obj.([]byte)))
	case byte:
		return  ToFloat(string(obj.(byte)))
	default:
		return de
	}
}

func ToFloatErr(obj interface{}) (float64,error) {
	switch obj.(type) {
	case string:
		i, err := strconv.ParseFloat(obj.(string),64)
		if(err != nil){
			return 0,err
		}
		return i,nil
	case int64:
		return float64(obj.(int64)),nil;
	case int:
		return ToFloat(int64(obj.(int))),nil
	case int8:
		return ToFloat(int64(obj.(int8))),nil
	case int16:
		return ToFloat(int64(obj.(int16))),nil
	case int32:
		return ToFloat(int64(obj.(int32))),nil
	case float64:
		return obj.(float64),nil
	case float32:
		return  ToFloat(float64(obj.(float32))),nil
	case []byte:
		return ToFloat(string(obj.([]byte))),nil
	case byte:
		return  ToFloat(string(obj.(byte))),nil
	default:
		var t=reflect.TypeOf(obj)
		if(t.Kind() == reflect.Ptr){
			t=t.Elem()
		}
		println(t.Kind().String())
		return 0,errors.New("error type")
	}
}

func StringPt(string2 string) *string {
	return &string2
}

func FloatPt(string2 float64) *float64 {
	return &string2
}

func FloatDecimal(value float64,count int) float64 {
	value, _ = strconv.ParseFloat(fmt.Sprintf("%."+ToString(count)+"f", value), 64)
	return value
}