package DateKit

import "time"

var Datetime="2006-01-02 15:04:05"

var GoDatetime="2006-01-02T15:04:05Z07:00"

var Day="2006-01-02"

var DayNo="20060102"

var codetime="20060102150405"

func GetDateTime(timestamp int64) string {
	return   time.Unix(timestamp, 0).Format(Datetime)
}


func GetDayNo(timestamp int64) string {
	return   time.Unix(timestamp, 0).Format(DayNo)
}


func GetDay(timestamp int64) string {
	return   time.Unix(timestamp, 0).Format(Day)
}

func GetCodeTime(timestamp int64) string {
	return   time.Unix(timestamp, 0).Format(codetime)
}


func GetDateTimeParse(timestamp string) (time.Time,error) {
	return   time.ParseInLocation(Datetime,timestamp,time.Local)
}


func GetDayParse(timestamp string) (time.Time,error) {
	return   GetDateTimeParse(timestamp+" 00:00:00")
}


func GetDateTimeParsenoError(timestamp string) time.Time {
	if t,err:=time.ParseInLocation(Datetime,timestamp,time.Local);err !=nil{
		panic(err.Error())
	}else {
		return  t
	}
}

func GetDayParsenoError(timestamp string) time.Time {
	return   GetDateTimeParsenoError(timestamp+" 00:00:00")
}

func ToTime(timestamp int64) time.Time {
	return   time.Unix(timestamp,0)
}


var weekDayMap = map[string]int{
	"Monday":    1,
	"Tuesday":   2,
	"Wednesday": 3,
	"Thursday":  4,
	"Friday":    5,
	"Saturday":  6,
	"Sunday":    7,
}


func GetWeek() int {
	wd := time.Now().Weekday().String()
	return weekDayMap[wd]
}