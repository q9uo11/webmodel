package sql

import (
	"errors"
	"reflect"
	"strings"
)

func  ToSQl (sql string,value string ) []interface{} {
	var m=[]interface{}{}
	n := strings.Count(sql, "?")
	for i := 0; i < n; i++ {
		m=append(m,value)
	}
	return m
}

func CheckOrder(order string)  {
	order=strings.Trim(order," ")
	//println(order)
	var s=strings.Split(order," ")
	//println(len(s))
	if(len(s) > 2){
		panic(errors.New("排序错误"))
	}
}

func GetSeach(obj interface{},tablename string) string {
	formStruct:=reflect.New(reflect.TypeOf(obj))
	if formStruct.Kind() == reflect.Ptr {
		formStruct = formStruct.Elem()
	}
	objtype:=formStruct.Type()
	var str=""
	for k := 0; k < objtype.NumField(); k++ {
		name:=strings.ToLower(objtype.Field(k).Name)
		if(objtype.Field(k).Tag.Get("json") != ""){
			name=objtype.Field(k).Tag.Get("json")
		}
		if(objtype.Field(k).Tag.Get("xorm") == "-"){
			continue
		}
		if(k != 0){
			str+=","
		}
		if(tablename == ""){
			str+="`"+name+"`"
		}else {
			str+=tablename+"."+name
		}
	}
	return str
}



func GetWhereSeach(obj interface{},tablename string,sname string) string {
	formStruct:=reflect.New(reflect.TypeOf(obj))
	if formStruct.Kind() == reflect.Ptr {
		formStruct = formStruct.Elem()
	}
	objtype:=formStruct.Type()
	var str=" "
	for k := 0; k < objtype.NumField(); k++ {
		name:=strings.ToLower(objtype.Field(k).Name)
		if(objtype.Field(k).Tag.Get("json") != ""){
			name=objtype.Field(k).Tag.Get("json")
		}
		if(k != 0){
			str+=" or "
		}

		if(tablename == ""){
			str+="`"+name+"`"
		}else {
			str+=tablename+"."+name
		}
		str+= " like binary "
		if(sname == ""){
			str+=" ? "
		}else {
			str+=" ?"+sname+" "
		}
	}
	return str
}

func GetWhereSeach2(obj interface{},tablename string,sname string) string {
	formStruct:=reflect.New(reflect.TypeOf(obj))
	if formStruct.Kind() == reflect.Ptr {
		formStruct = formStruct.Elem()
	}
	objtype:=formStruct.Type()
	var str=" "
	for k := 0; k < objtype.NumField(); k++ {
		name:=strings.ToLower(objtype.Field(k).Name)
		if(objtype.Field(k).Tag.Get("json") != ""){
			name=objtype.Field(k).Tag.Get("json")
		}
		if(k != 0){
			str+=" or "
		}

		if(tablename == ""){
			str+="`"+name+"`"
		}else {
			str+=tablename+"."+name
		}
		str+= " like "
		if(sname == ""){
			str+=" ? "
		}else {
			str+=" ?"+sname+" "
		}
	}
	return str
}
