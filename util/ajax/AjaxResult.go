package ajax

type AjaxResult struct {
	Code int  `json:"code"`
	Msg string `json:"msg"`
	Data interface{} `json:"data"`
}

func (a AjaxResult) AddError(msg string)  AjaxResult{
	return AjaxResult{Code:-1,Msg:msg}
}


func (a AjaxResult) Success()  AjaxResult{
	return AjaxResult{Code:0}
}